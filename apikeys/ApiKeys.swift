//
//  ApiKeys.swift
//  apikeys
//
//  Copyright (c) 2014 iAchieved.it LLC. All rights reserved.
//

import Foundation

func valueForAPIKey(#keyname:String) -> String {
  let filePath = NSBundle.mainBundle().pathForResource("ApiKeys", ofType:"plist")
  let plist = NSDictionary(contentsOfFile:filePath!)
  
  let value:String = plist?.objectForKey(keyname) as String
  return value
}

